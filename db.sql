+USE [master]
+GO
+
+/****** Object:  Database [HomeBanking]    Script Date: 22/06/2020 22:08:34 ******/
+CREATE DATABASE [HomeBanking]
+ CONTAINMENT = NONE
+ ON  PRIMARY 
+( NAME = N'HomeBanking', FILENAME = N'F:\sql\MSSQL15.SQLEXPRESS01\MSSQL\DATA\HomeBanking.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
+ LOG ON 
+( NAME = N'HomeBanking_log', FILENAME = N'F:\sql\MSSQL15.SQLEXPRESS01\MSSQL\DATA\HomeBanking_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
+ WITH CATALOG_COLLATION = DATABASE_DEFAULT
+GO
+
+IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
+begin
+EXEC [HomeBanking].[dbo].[sp_fulltext_database] @action = 'enable'
+end
+GO
+
+ALTER DATABASE [HomeBanking] SET ANSI_NULL_DEFAULT OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET ANSI_NULLS OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET ANSI_PADDING OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET ANSI_WARNINGS OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET ARITHABORT OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET AUTO_CLOSE OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET AUTO_SHRINK OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET AUTO_UPDATE_STATISTICS ON 
+GO
+
+ALTER DATABASE [HomeBanking] SET CURSOR_CLOSE_ON_COMMIT OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET CURSOR_DEFAULT  GLOBAL 
+GO
+
+ALTER DATABASE [HomeBanking] SET CONCAT_NULL_YIELDS_NULL OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET NUMERIC_ROUNDABORT OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET QUOTED_IDENTIFIER OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET RECURSIVE_TRIGGERS OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET  DISABLE_BROKER 
+GO
+
+ALTER DATABASE [HomeBanking] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET DATE_CORRELATION_OPTIMIZATION OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET TRUSTWORTHY OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET ALLOW_SNAPSHOT_ISOLATION OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET PARAMETERIZATION SIMPLE 
+GO
+
+ALTER DATABASE [HomeBanking] SET READ_COMMITTED_SNAPSHOT OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET HONOR_BROKER_PRIORITY OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET RECOVERY SIMPLE 
+GO
+
+ALTER DATABASE [HomeBanking] SET  MULTI_USER 
+GO
+
+ALTER DATABASE [HomeBanking] SET PAGE_VERIFY CHECKSUM  
+GO
+
+ALTER DATABASE [HomeBanking] SET DB_CHAINING OFF 
+GO
+
+ALTER DATABASE [HomeBanking] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
+GO
+
+ALTER DATABASE [HomeBanking] SET TARGET_RECOVERY_TIME = 60 SECONDS 
+GO
+
+ALTER DATABASE [HomeBanking] SET DELAYED_DURABILITY = DISABLED 
+GO
+
+ALTER DATABASE [HomeBanking] SET QUERY_STORE = OFF
+GO
+
+ALTER DATABASE [HomeBanking] SET  READ_WRITE 
+GO
+
+
+USE [HomeBanking]
+GO
+
+/****** Object:  Table [dbo].[historia]    Script Date: 22/06/2020 22:09:02 ******/
+SET ANSI_NULLS ON
+GO
+
+SET QUOTED_IDENTIFIER ON
+GO
+
+CREATE TABLE [dbo].[historia](
+	[id] [int] NOT NULL,
+	[idusuario] [nchar](10) NOT NULL,
+	[mes] [int] NOT NULL,
+	[a�o] [int] NOT NULL,
+	[cantidad] [float] NOT NULL
+) ON [PRIMARY]
+GO
+
+USE [HomeBanking]
+GO
+
+/****** Object:  Table [dbo].[inversiones]    Script Date: 22/06/2020 22:09:15 ******/
+SET ANSI_NULLS ON
+GO
+
+SET QUOTED_IDENTIFIER ON
+GO
+
+CREATE TABLE [dbo].[inversiones](
+	[id] [int] NOT NULL,
+	[idusuario] [int] NOT NULL,
+	[uva] [bit] NOT NULL,
+	[monto] [float] NOT NULL,
+	[fechaInicio] [date] NOT NULL,
+	[fechaFinal] [date] NOT NULL,
+	[plazo] [int] NOT NULL,
+	[estado] [bit] NOT NULL,
+	[total] [float] NOT NULL
+) ON [PRIMARY]
+GO
+
+ALTER TABLE [dbo].[inversiones] ADD  CONSTRAINT [DF_inversiones1_estado]  DEFAULT ((0)) FOR [estado]
+GO
+
+ALTER TABLE [dbo].[inversiones] ADD  CONSTRAINT [DF_inversiones1_interes]  DEFAULT ((0)) FOR [total]
+GO
+
+
+USE [HomeBanking]
+GO
+
+/****** Object:  Table [dbo].[users]    Script Date: 22/06/2020 22:09:21 ******/
+SET ANSI_NULLS ON
+GO
+
+SET QUOTED_IDENTIFIER ON
+GO
+
+CREATE TABLE [dbo].[users](
+	[id] [int] NOT NULL,
+	[cuenta] [nchar](12) NOT NULL,
+	[saldoPesos] [float] NOT NULL,
+	[saldoDolares] [float] NOT NULL,
+	[contrase�a] [nchar](50) NOT NULL
+) ON [PRIMARY]
+GO
+