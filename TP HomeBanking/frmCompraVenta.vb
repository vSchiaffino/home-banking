﻿Public Class frmCompraVenta
    Dim valorCompra As Double
    Dim valorVenta As Double
    Dim us As user
    Dim cantidad As Double
    Dim operacion As String
    Dim valorEP As Double = 0
    Dim impuesto As Double = 0
    Dim total As Double

    Public Sub Init(u As Object)
        u = CType(u, user)
        us = u
    End Sub


    Private Sub frmCompraVenta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'me fijo a cuanto esta el dolar
        gbDetalles.Visible = False
        Height = 314
        Dim webClient As New Net.WebClient
        Dim result As String = webClient.DownloadString("https://dolarhoy.com")
        Dim encontre As Integer = 0
        For i = 0 To result.Length
            If i + 8 > result.Length - 1 Then
                Exit For
                MessageBox.Show("No se a cuanto esta el dolar che")
            End If
            Dim look As String = result.Substring(i, 8)
            Dim looking_for As String = Chr(34) + "price" + Chr(34) + ">"
            If look = looking_for Then
                Dim j As Integer = i
                Dim estado As String = "leyendo"
                Dim valor_final As String = ""
                While True
                    Dim caracter As String = result.Substring(j, 1)
                    If estado = "leyendo" Then
                        If caracter = ">" Then
                            estado = "escribiendo"
                        End If
                    ElseIf estado = "escribiendo" Then
                        If caracter = "<" Then
                            Exit While
                        End If
                        If caracter = "," Or IsNumeric(caracter) Then
                            If caracter = "," Then
                                caracter = "."
                            End If
                            valor_final += caracter
                        End If
                    End If
                    j += 1
                End While
                valor_final = valor_final.Replace("$", "")
                valor_final = valor_final.Replace(",", ".")
                If encontre = 0 Then
                    valorCompra = CDbl(Val(valor_final))
                ElseIf encontre = 1 Then
                    valorVenta = CDbl(Val(valor_final))
                End If
                encontre += 1
                If encontre = 2 Then
                    Exit For
                End If
            End If
        Next
        'lleno txts
        txtCompra.Text = "$" & valorCompra
        txtVenta.Text = "$" & valorVenta
        txtDolares.Text = "US$" & us.saldoDolares
        txtPesos.Text = "$" & us.saldoPesos
    End Sub
    Private Sub ActualizarUser()
        txtDolares.Text = "US$" & us.saldoDolares
        txtPesos.Text = "$" & us.saldoPesos
    End Sub

    Private Sub cmdDetalles_Click(sender As Object, e As EventArgs) Handles cmdDetalles.Click
        'checkeo si las cosas tan bien
        valorEP = 0
        impuesto = 0
        cantidad = CDbl(Val(txtCantidad.Text))
        If optVenta.Checked Then
            operacion = "v"
        Else
            operacion = "c"
        End If
        If optVenta.Checked = False And optCompra.Checked = False Then
            MessageBox.Show("Por favor elija un tipo de operacion.")
            Exit Sub
        End If
        If txtCantidad.Text = "" Then
            MessageBox.Show("Por favor ingrese una cantidad.")
            Exit Sub
        End If
        If Not IsNumeric(txtCantidad.Text) Then
            MessageBox.Show("Ingrese una cantidad valida.")
            Exit Sub
        End If
        If operacion = "v" Then
            If us.saldoDolares < cantidad Then
                MessageBox.Show("Error. Usted no posee esa cifra de dolares.")
            End If

        Else
            total = cantidad * valorCompra
            total = total + total * 0.3
            If total > us.saldoPesos Then
                MessageBox.Show("Error. Usted no posee esa cifra de pesos.")
                Exit Sub
            End If
        End If



        'tan bien
        gbDetalles.Visible = True
        cmdDetalles.Enabled = False
        optCompra.Enabled = False
        optVenta.Enabled = False
        txtCantidad.Enabled = False
        Height = 558
        cantidad = CDbl(Val(txtCantidad.Text))
        If operacion = "c" Then
            lblTotal.Text = "Total a debitar"
            lblImpuesto.Visible = True
            lblTipo.Text = "Compra de dolares."
            valorEP = cantidad * valorVenta
            impuesto = valorEP * 30 / 100
            txtImpuesto.Text = "$" & impuesto
            txtImpuesto.Visible = True
            total = valorEP + impuesto
        Else
            lblTotal.Text = "Total a acreditar"
            lblImpuesto.Visible = False
            txtImpuesto.Visible = False
            valorEP = cantidad * valorCompra
            lblTipo.Text = "Venta de dolares."
            total = valorEP
        End If
        txtValorEnPesos.Text = "$" & valorEP
        txtCantidad2.Text = "US$" & txtCantidad.Text
        txtTotal.Text = "$" & total
    End Sub

    Private Sub cmdConfirmar_Click(sender As Object, e As EventArgs) Handles cmdConfirmar.Click
        Height = 314
        cmdDetalles.Enabled = True
        optCompra.Enabled = True
        optVenta.Enabled = True
        txtCantidad.Enabled = True
        gbDetalles.Visible = False
        txtCantidad.Text = ""
        optCompra.Checked = False
        optVenta.Checked = False
        Dim r = HandleOperation(us.id, operacion, cantidad, total)
        If r Is Nothing Then
            MessageBox.Show("Usted no puede efectuar esa operación. Excedió el cupo por mes.")
        Else
            us = r
            ActualizarUser()
        End If
    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        Height = 314
        cmdDetalles.Enabled = True
        gbDetalles.Visible = False
        optCompra.Enabled = True
        optVenta.Enabled = True
        txtCantidad.Enabled = True
        txtCantidad.Text = ""
        optCompra.Checked = False
        optVenta.Checked = False
    End Sub
End Class