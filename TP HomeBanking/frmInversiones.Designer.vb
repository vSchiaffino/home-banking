﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInversiones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.optNormal = New System.Windows.Forms.RadioButton()
        Me.optUVA = New System.Windows.Forms.RadioButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtCapital = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cbPlazo = New System.Windows.Forms.ComboBox()
        Me.cmdDetalles = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtUVA = New System.Windows.Forms.TextBox()
        Me.lblUva = New System.Windows.Forms.Label()
        Me.gbDetalles = New System.Windows.Forms.GroupBox()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.cmdConfirmar = New System.Windows.Forms.Button()
        Me.lblMontoF = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblDias = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblMonto = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtTotalUVA = New System.Windows.Forms.TextBox()
        Me.lblTEU = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.gbDetalles.SuspendLayout()
        Me.SuspendLayout()
        '
        'optNormal
        '
        Me.optNormal.AutoSize = True
        Me.optNormal.Checked = True
        Me.optNormal.Location = New System.Drawing.Point(6, 19)
        Me.optNormal.Name = "optNormal"
        Me.optNormal.Size = New System.Drawing.Size(101, 17)
        Me.optNormal.TabIndex = 0
        Me.optNormal.TabStop = True
        Me.optNormal.Text = "Plazo fijo normal"
        Me.optNormal.UseVisualStyleBackColor = True
        '
        'optUVA
        '
        Me.optUVA.AutoSize = True
        Me.optUVA.Location = New System.Drawing.Point(113, 19)
        Me.optUVA.Name = "optUVA"
        Me.optUVA.Size = New System.Drawing.Size(92, 17)
        Me.optUVA.TabIndex = 1
        Me.optUVA.Text = "Plazo fijo UVA"
        Me.optUVA.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 53)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Capital a invertir:"
        '
        'txtCapital
        '
        Me.txtCapital.Location = New System.Drawing.Point(97, 50)
        Me.txtCapital.Name = "txtCapital"
        Me.txtCapital.Size = New System.Drawing.Size(200, 20)
        Me.txtCapital.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Plazo"
        '
        'cbPlazo
        '
        Me.cbPlazo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbPlazo.FormattingEnabled = True
        Me.cbPlazo.Location = New System.Drawing.Point(97, 83)
        Me.cbPlazo.Name = "cbPlazo"
        Me.cbPlazo.Size = New System.Drawing.Size(231, 21)
        Me.cbPlazo.TabIndex = 5
        '
        'cmdDetalles
        '
        Me.cmdDetalles.Location = New System.Drawing.Point(160, 113)
        Me.cmdDetalles.Name = "cmdDetalles"
        Me.cmdDetalles.Size = New System.Drawing.Size(75, 34)
        Me.cmdDetalles.TabIndex = 6
        Me.cmdDetalles.Text = "Ver detalles"
        Me.cmdDetalles.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblTEU)
        Me.GroupBox1.Controls.Add(Me.txtTotalUVA)
        Me.GroupBox1.Controls.Add(Me.txtUVA)
        Me.GroupBox1.Controls.Add(Me.lblUva)
        Me.GroupBox1.Controls.Add(Me.optNormal)
        Me.GroupBox1.Controls.Add(Me.cmdDetalles)
        Me.GroupBox1.Controls.Add(Me.optUVA)
        Me.GroupBox1.Controls.Add(Me.cbPlazo)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtCapital)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(495, 153)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Elegí tus opciones"
        '
        'txtUVA
        '
        Me.txtUVA.Location = New System.Drawing.Point(285, 18)
        Me.txtUVA.Name = "txtUVA"
        Me.txtUVA.ReadOnly = True
        Me.txtUVA.Size = New System.Drawing.Size(79, 20)
        Me.txtUVA.TabIndex = 8
        Me.txtUVA.Visible = False
        '
        'lblUva
        '
        Me.lblUva.AutoSize = True
        Me.lblUva.Location = New System.Drawing.Point(215, 21)
        Me.lblUva.Name = "lblUva"
        Me.lblUva.Size = New System.Drawing.Size(64, 13)
        Me.lblUva.TabIndex = 7
        Me.lblUva.Text = "UVA actual:"
        Me.lblUva.Visible = False
        '
        'gbDetalles
        '
        Me.gbDetalles.Controls.Add(Me.cmdCancelar)
        Me.gbDetalles.Controls.Add(Me.cmdConfirmar)
        Me.gbDetalles.Controls.Add(Me.lblMontoF)
        Me.gbDetalles.Controls.Add(Me.Label9)
        Me.gbDetalles.Controls.Add(Me.lblFecha)
        Me.gbDetalles.Controls.Add(Me.Label7)
        Me.gbDetalles.Controls.Add(Me.lblDias)
        Me.gbDetalles.Controls.Add(Me.Label4)
        Me.gbDetalles.Controls.Add(Me.lblMonto)
        Me.gbDetalles.Controls.Add(Me.Label3)
        Me.gbDetalles.Location = New System.Drawing.Point(71, 172)
        Me.gbDetalles.Name = "gbDetalles"
        Me.gbDetalles.Size = New System.Drawing.Size(276, 163)
        Me.gbDetalles.TabIndex = 8
        Me.gbDetalles.TabStop = False
        Me.gbDetalles.Text = "Detalles"
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Location = New System.Drawing.Point(60, 117)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(75, 40)
        Me.cmdCancelar.TabIndex = 9
        Me.cmdCancelar.Text = "Cancelar"
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'cmdConfirmar
        '
        Me.cmdConfirmar.Location = New System.Drawing.Point(141, 117)
        Me.cmdConfirmar.Name = "cmdConfirmar"
        Me.cmdConfirmar.Size = New System.Drawing.Size(75, 40)
        Me.cmdConfirmar.TabIndex = 8
        Me.cmdConfirmar.Text = "Confirmar"
        Me.cmdConfirmar.UseVisualStyleBackColor = True
        '
        'lblMontoF
        '
        Me.lblMontoF.AutoSize = True
        Me.lblMontoF.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMontoF.Location = New System.Drawing.Point(133, 94)
        Me.lblMontoF.Name = "lblMontoF"
        Me.lblMontoF.Size = New System.Drawing.Size(83, 15)
        Me.lblMontoF.TabIndex = 7
        Me.lblMontoF.Text = "$ 25.546,58"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(15, 95)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(59, 13)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Monto final"
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.Location = New System.Drawing.Point(133, 71)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(79, 15)
        Me.lblFecha.TabIndex = 5
        Me.lblFecha.Text = "12/06/2020"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(15, 72)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(112, 13)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Fecha de vencimiento"
        '
        'lblDias
        '
        Me.lblDias.AutoSize = True
        Me.lblDias.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDias.Location = New System.Drawing.Point(133, 48)
        Me.lblDias.Name = "lblDias"
        Me.lblDias.Size = New System.Drawing.Size(54, 15)
        Me.lblDias.TabIndex = 3
        Me.lblDias.Text = "30 dias"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(15, 49)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(33, 13)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Plazo"
        '
        'lblMonto
        '
        Me.lblMonto.AutoSize = True
        Me.lblMonto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMonto.Location = New System.Drawing.Point(133, 25)
        Me.lblMonto.Name = "lblMonto"
        Me.lblMonto.Size = New System.Drawing.Size(87, 15)
        Me.lblMonto.TabIndex = 1
        Me.lblMonto.Text = "$ 25.000, 00"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(15, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Monto inicial"
        '
        'txtTotalUVA
        '
        Me.txtTotalUVA.Location = New System.Drawing.Point(386, 50)
        Me.txtTotalUVA.Name = "txtTotalUVA"
        Me.txtTotalUVA.Size = New System.Drawing.Size(103, 20)
        Me.txtTotalUVA.TabIndex = 9
        '
        'lblTEU
        '
        Me.lblTEU.AutoSize = True
        Me.lblTEU.Location = New System.Drawing.Point(304, 53)
        Me.lblTEU.Name = "lblTEU"
        Me.lblTEU.Size = New System.Drawing.Size(76, 13)
        Me.lblTEU.TabIndex = 10
        Me.lblTEU.Text = "Total en UVAs"
        Me.lblTEU.Visible = False
        '
        'frmInversiones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(519, 340)
        Me.Controls.Add(Me.gbDetalles)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmInversiones"
        Me.Text = "Inversiones"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.gbDetalles.ResumeLayout(False)
        Me.gbDetalles.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents optNormal As RadioButton
    Friend WithEvents optUVA As RadioButton
    Friend WithEvents Label1 As Label
    Friend WithEvents txtCapital As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents cbPlazo As ComboBox
    Friend WithEvents cmdDetalles As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtUVA As TextBox
    Friend WithEvents lblUva As Label
    Friend WithEvents gbDetalles As GroupBox
    Friend WithEvents cmdConfirmar As Button
    Friend WithEvents lblMontoF As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents lblFecha As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents lblDias As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lblMonto As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents cmdCancelar As Button
    Friend WithEvents lblTEU As Label
    Friend WithEvents txtTotalUVA As TextBox
End Class
