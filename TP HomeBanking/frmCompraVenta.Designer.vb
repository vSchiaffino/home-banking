﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCompraVenta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtVenta = New System.Windows.Forms.TextBox()
        Me.txtCompra = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtDolares = New System.Windows.Forms.TextBox()
        Me.txtPesos = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cmdDetalles = New System.Windows.Forms.Button()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.optVenta = New System.Windows.Forms.RadioButton()
        Me.optCompra = New System.Windows.Forms.RadioButton()
        Me.gbDetalles = New System.Windows.Forms.GroupBox()
        Me.cmdCancelar = New System.Windows.Forms.Button()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.txtImpuesto = New System.Windows.Forms.TextBox()
        Me.lblImpuesto = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtValorEnPesos = New System.Windows.Forms.TextBox()
        Me.lblTip = New System.Windows.Forms.Label()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.cmdConfirmar = New System.Windows.Forms.Button()
        Me.txtCantidad2 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.gbDetalles.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtVenta)
        Me.GroupBox1.Controls.Add(Me.txtCompra)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(159, 88)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Cotización del dolar"
        '
        'txtVenta
        '
        Me.txtVenta.Location = New System.Drawing.Point(57, 56)
        Me.txtVenta.Name = "txtVenta"
        Me.txtVenta.ReadOnly = True
        Me.txtVenta.Size = New System.Drawing.Size(90, 20)
        Me.txtVenta.TabIndex = 3
        '
        'txtCompra
        '
        Me.txtCompra.Location = New System.Drawing.Point(57, 29)
        Me.txtCompra.Name = "txtCompra"
        Me.txtCompra.ReadOnly = True
        Me.txtCompra.Size = New System.Drawing.Size(90, 20)
        Me.txtCompra.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 59)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Venta"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Compra"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtDolares)
        Me.GroupBox2.Controls.Add(Me.txtPesos)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Location = New System.Drawing.Point(189, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(159, 88)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Saldo de la cuenta"
        '
        'txtDolares
        '
        Me.txtDolares.Location = New System.Drawing.Point(57, 56)
        Me.txtDolares.Name = "txtDolares"
        Me.txtDolares.ReadOnly = True
        Me.txtDolares.Size = New System.Drawing.Size(90, 20)
        Me.txtDolares.TabIndex = 3
        '
        'txtPesos
        '
        Me.txtPesos.Location = New System.Drawing.Point(57, 29)
        Me.txtPesos.Name = "txtPesos"
        Me.txtPesos.ReadOnly = True
        Me.txtPesos.Size = New System.Drawing.Size(90, 20)
        Me.txtPesos.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Dolares"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(7, 32)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(36, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Pesos"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 25)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(124, 13)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Detalles de la operación:"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cmdDetalles)
        Me.GroupBox3.Controls.Add(Me.txtCantidad)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.optVenta)
        Me.GroupBox3.Controls.Add(Me.optCompra)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 106)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(336, 163)
        Me.GroupBox3.TabIndex = 4
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Operación"
        '
        'cmdDetalles
        '
        Me.cmdDetalles.Location = New System.Drawing.Point(127, 123)
        Me.cmdDetalles.Name = "cmdDetalles"
        Me.cmdDetalles.Size = New System.Drawing.Size(93, 34)
        Me.cmdDetalles.TabIndex = 11
        Me.cmdDetalles.Text = "Ver detalles de la operación"
        Me.cmdDetalles.UseVisualStyleBackColor = True
        '
        'txtCantidad
        '
        Me.txtCantidad.Location = New System.Drawing.Point(108, 81)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(119, 20)
        Me.txtCantidad.TabIndex = 10
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 84)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 13)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Cantidad"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 55)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(96, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Tipo de operación:"
        '
        'optVenta
        '
        Me.optVenta.AutoSize = True
        Me.optVenta.Location = New System.Drawing.Point(175, 53)
        Me.optVenta.Name = "optVenta"
        Me.optVenta.Size = New System.Drawing.Size(53, 17)
        Me.optVenta.TabIndex = 7
        Me.optVenta.TabStop = True
        Me.optVenta.Text = "Venta"
        Me.optVenta.UseVisualStyleBackColor = True
        '
        'optCompra
        '
        Me.optCompra.AutoSize = True
        Me.optCompra.Location = New System.Drawing.Point(108, 53)
        Me.optCompra.Name = "optCompra"
        Me.optCompra.Size = New System.Drawing.Size(61, 17)
        Me.optCompra.TabIndex = 6
        Me.optCompra.TabStop = True
        Me.optCompra.Text = "Compra"
        Me.optCompra.UseVisualStyleBackColor = True
        '
        'gbDetalles
        '
        Me.gbDetalles.Controls.Add(Me.cmdCancelar)
        Me.gbDetalles.Controls.Add(Me.txtTotal)
        Me.gbDetalles.Controls.Add(Me.lblTotal)
        Me.gbDetalles.Controls.Add(Me.txtImpuesto)
        Me.gbDetalles.Controls.Add(Me.lblImpuesto)
        Me.gbDetalles.Controls.Add(Me.Label11)
        Me.gbDetalles.Controls.Add(Me.txtValorEnPesos)
        Me.gbDetalles.Controls.Add(Me.lblTip)
        Me.gbDetalles.Controls.Add(Me.lblTipo)
        Me.gbDetalles.Controls.Add(Me.cmdConfirmar)
        Me.gbDetalles.Controls.Add(Me.txtCantidad2)
        Me.gbDetalles.Controls.Add(Me.Label8)
        Me.gbDetalles.Location = New System.Drawing.Point(12, 275)
        Me.gbDetalles.Name = "gbDetalles"
        Me.gbDetalles.Size = New System.Drawing.Size(336, 238)
        Me.gbDetalles.TabIndex = 12
        Me.gbDetalles.TabStop = False
        Me.gbDetalles.Text = "Detalles"
        '
        'cmdCancelar
        '
        Me.cmdCancelar.Location = New System.Drawing.Point(76, 198)
        Me.cmdCancelar.Name = "cmdCancelar"
        Me.cmdCancelar.Size = New System.Drawing.Size(93, 34)
        Me.cmdCancelar.TabIndex = 20
        Me.cmdCancelar.Text = "Cancelar"
        Me.cmdCancelar.UseVisualStyleBackColor = True
        '
        'txtTotal
        '
        Me.txtTotal.Location = New System.Drawing.Point(101, 153)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.ReadOnly = True
        Me.txtTotal.Size = New System.Drawing.Size(119, 20)
        Me.txtTotal.TabIndex = 19
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(7, 156)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(31, 13)
        Me.lblTotal.TabIndex = 18
        Me.lblTotal.Text = "Total"
        '
        'txtImpuesto
        '
        Me.txtImpuesto.Location = New System.Drawing.Point(101, 108)
        Me.txtImpuesto.Name = "txtImpuesto"
        Me.txtImpuesto.ReadOnly = True
        Me.txtImpuesto.Size = New System.Drawing.Size(119, 20)
        Me.txtImpuesto.TabIndex = 17
        '
        'lblImpuesto
        '
        Me.lblImpuesto.AutoSize = True
        Me.lblImpuesto.Location = New System.Drawing.Point(7, 112)
        Me.lblImpuesto.Name = "lblImpuesto"
        Me.lblImpuesto.Size = New System.Drawing.Size(77, 13)
        Me.lblImpuesto.TabIndex = 16
        Me.lblImpuesto.Text = "Impuesto PAIS"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(6, 82)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(77, 13)
        Me.Label11.TabIndex = 15
        Me.Label11.Text = "Valor en pesos"
        '
        'txtValorEnPesos
        '
        Me.txtValorEnPesos.Location = New System.Drawing.Point(101, 78)
        Me.txtValorEnPesos.Name = "txtValorEnPesos"
        Me.txtValorEnPesos.ReadOnly = True
        Me.txtValorEnPesos.Size = New System.Drawing.Size(119, 20)
        Me.txtValorEnPesos.TabIndex = 14
        '
        'lblTip
        '
        Me.lblTip.AutoSize = True
        Me.lblTip.Location = New System.Drawing.Point(7, 25)
        Me.lblTip.Name = "lblTip"
        Me.lblTip.Size = New System.Drawing.Size(28, 13)
        Me.lblTip.TabIndex = 13
        Me.lblTip.Text = "Tipo"
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Location = New System.Drawing.Point(105, 25)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(98, 13)
        Me.lblTipo.TabIndex = 12
        Me.lblTipo.Text = "Compra de dolares."
        '
        'cmdConfirmar
        '
        Me.cmdConfirmar.Location = New System.Drawing.Point(175, 198)
        Me.cmdConfirmar.Name = "cmdConfirmar"
        Me.cmdConfirmar.Size = New System.Drawing.Size(93, 34)
        Me.cmdConfirmar.TabIndex = 11
        Me.cmdConfirmar.Text = "Confirmar operación"
        Me.cmdConfirmar.UseVisualStyleBackColor = True
        '
        'txtCantidad2
        '
        Me.txtCantidad2.Location = New System.Drawing.Point(101, 48)
        Me.txtCantidad2.Name = "txtCantidad2"
        Me.txtCantidad2.ReadOnly = True
        Me.txtCantidad2.Size = New System.Drawing.Size(119, 20)
        Me.txtCantidad2.TabIndex = 10
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 52)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(49, 13)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "Cantidad"
        '
        'frmCompraVenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(367, 519)
        Me.Controls.Add(Me.gbDetalles)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmCompraVenta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Compra-venta dolares"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.gbDetalles.ResumeLayout(False)
        Me.gbDetalles.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtVenta As TextBox
    Friend WithEvents txtCompra As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents txtDolares As TextBox
    Friend WithEvents txtPesos As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents cmdDetalles As Button
    Friend WithEvents txtCantidad As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents optVenta As RadioButton
    Friend WithEvents optCompra As RadioButton
    Friend WithEvents gbDetalles As GroupBox
    Friend WithEvents txtTotal As TextBox
    Friend WithEvents lblTotal As Label
    Friend WithEvents txtImpuesto As TextBox
    Friend WithEvents lblImpuesto As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents txtValorEnPesos As TextBox
    Friend WithEvents lblTip As Label
    Friend WithEvents lblTipo As Label
    Friend WithEvents cmdConfirmar As Button
    Friend WithEvents txtCantidad2 As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents cmdCancelar As Button
End Class
