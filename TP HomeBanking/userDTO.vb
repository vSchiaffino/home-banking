﻿Module userDTO
    Class user
        Public Property id As Integer
        Public Property cuenta As String
        Public Property saldoPesos As Decimal
        Public Property saldoDolares As Decimal
        Public Property contraseña As String
        Public Property cupo As Double
    End Class
End Module
