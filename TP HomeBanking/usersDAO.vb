﻿
Module usersDAO

    Function HandleOperation(id As Integer, op As String, cantidad As Double, total As Double) As user
        Dim u = getUser(id)

        Dim debitoPesos As Double = 0, debitoDolares As Double = 0
        If op = "c" Then
            ' veo si puede comprarlo
            If u.saldoPesos < total Then
                Return Nothing
            End If
            ' Veo si excede el max de dolares comprados el mes
            If cantidad + u.cupo > 200 Then
                Return Nothing
            End If
            'todo piola
            debitoDolares = cantidad
            debitoPesos = -total
        ElseIf op = "v" Then
            'veo si puede venderlo
            If u.saldoDolares < cantidad Then
                Return Nothing
            End If
            debitoDolares = -cantidad
            debitoPesos = total
        End If
        Dim cmdtext = "UPDATE USERS SET saldoPesos = saldoPesos + " & debitoPesos & " º saldoDolares = saldoDolares + " & debitoDolares
        cmdtext = cmdtext.Replace(",", ".")
        cmdtext = cmdtext.Replace("º", ",")
        DoCMD(cmdtext)
        If op = "c" Then
            Dim c As String = cantidad
            c = c.Replace(",", ".")
            Dim hoy = Today
            Dim mes = hoy.Month
            Dim año = hoy.Year
            cmdtext = "INSERT INTO historia(id, idusuario, año, mes, cantidad) VALUES (" & GetNextId("historia") & ", " & u.id & ", " & año & ", " & mes & ", " & c & ")"
            DoCMD(cmdtext)
        End If
        Return getUser(id)
    End Function



    Function getUser(id As Integer) As user
        Return LlenarDTO(DoQuery("SELECT * from users where id = " & id)).Item(0)
    End Function

    Sub ActualizarPesos(id As Integer, cantidad As Double)
        Dim cmdText As String = "UPDATE users SET saldoPesos = saldoPesos + " & cantidad
        cmdText = cmdText.Replace(",", ".")
        DoCMD(cmdText)
    End Sub

    Sub ActualizarDolares(id As Integer, cantidad As Double)
        Dim cmdText As String = "UPDATE users SET saldoDolares = saldoDolares + " & cantidad
        cmdText = cmdText.Replace(",", ".")
        DoCMD(cmdText)
    End Sub

    Function ValidarUsuario(u As user) As user
        Dim query As String = "SELECT * FROM users WHERE cuenta = '" + u.cuenta + "'"
        Dim users As List(Of user) = LlenarDTO(DoQuery(query))
        If users.Count = 1 Then
            Dim us As user = users.Item(0)
            If us.contraseña = u.contraseña Then
                Return us
            End If
        End If
        Return Nothing
    End Function

    Function CrearUsuario(u As user) As Boolean
        Dim id As Integer = GetNextId("users")
        Dim cmdText As String
        cmdText = "INSERT INTO users(id, cuenta, saldoPesos, saldoDolares, contraseña) VALUES(" & id & ", '" & u.cuenta & "',100000.00, 1000.00,'" & u.contraseña & "')"
        DoCMD(cmdText)
        Return True
    End Function

    Function LlenarDTO(dt As DataTable) As List(Of user)
        Dim users As List(Of user) = New List(Of user)
        For Each dr As DataRow In dt.Rows
            Dim u As user = New user
            u.id = Convert.ToInt32(dr.Item("id"))
            u.cuenta = Convert.ToString(LTrim(RTrim(dr.Item("cuenta"))))
            u.saldoDolares = Convert.ToDecimal(dr.Item("saldoDolares"))
            u.saldoPesos = Convert.ToDecimal(dr.Item("saldoPesos"))
            u.contraseña = Convert.ToString(LTrim(RTrim(dr.Item("contraseña"))))
            u.cupo = GetCupo(u.id)
            users.Add(u)
        Next
        Return users
    End Function

    Function GetCupo(idusuario As Integer) As Double
        Dim hoy = Today
        Dim mes = hoy.Month
        Dim año = hoy.Year
        Dim dt = DoQuery("SELECT SUM(cantidad) FROM historia WHERE idusuario = " & idusuario & " and mes = " & mes & " and año = " & año)
        If IsDBNull(dt.Rows.Item(0).Item(0)) Then
            Return 0
        End If
        Return Convert.ToDouble(dt.Rows.Item(0).Item(0))
    End Function

End Module
