﻿Public Class frmMisInversiones
    Dim user As user
    Dim selectedId = 0
    Public Sub Init(us As Object)
        user = CType(us, user)
    End Sub

    Sub actualizarTablas()
        grdOff.DataSource = GetInversionesTerminadas(user.id)
        grdOn.DataSource = GetInversionesEnCurso(user.id)
    End Sub

    Private Sub frmMisInversiones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        grdOn.AutoGenerateColumns = False
        grdOff.AutoGenerateColumns = False
        actualizarTablas()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim i As Inversion = GetInversion(selectedId)
        If i.uva Then
            Dim uva As Double
            If txtUVA.Text = "" Then
                uva = getUVA()

            Else
                uva = CDbl(Val(txtUVA.Text))
            End If
            i.total = i.monto * uva
        End If
        i.estado = True
        TerminarInversion(i)
        actualizarTablas()
    End Sub

    Private Sub txtUVA_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtUVA.KeyPress
        If Not ((Asc(e.KeyChar)) = 8 Or Asc(e.KeyChar) = 46 Or (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57)) Then
            e.KeyChar = Nothing
        End If
    End Sub

    Private Sub grdOn_RowEnter(sender As Object, e As DataGridViewCellEventArgs) Handles grdOn.RowEnter
        If grdOn.SelectedRows.Count > 0 Then
            Dim row = grdOn.SelectedRows.Item(0)
            selectedId = Convert.ToInt32(row.Cells("id").Value)
        End If
    End Sub

    Private Sub grdOn_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdOn.CellContentClick

    End Sub
End Class