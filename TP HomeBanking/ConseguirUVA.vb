﻿Module ConseguirUVA
    Function getUVA() As Double
        Dim valorstr = ""
        Dim webClient As New Net.WebClient
        Dim result As String = webClient.DownloadString("https://ikiwi.net.ar/valor-uva/")
        For i = 0 To result.Length
            If result.Substring(i, 46) = "<strong style=" & Chr(34) & "color:#009765; font-size:45px" & Chr(34) & ">" Then
                i = i + 46
                Dim j = i
                While True
                    Dim parte = result.Substring(j, 1)
                    If parte = "<" Then
                        Exit While
                    End If
                    If Not parte = "$" Then
                        valorstr += parte
                    End If
                    j += 1
                End While
                Return CDbl(Val(valorstr.Replace(",", ".")))
                Exit For
            End If
        Next
    End Function

End Module
