﻿Imports System.Data.SqlClient

Module DAOHelper
    Function DoQuery(query As String) As DataTable
        Dim dt As DataTable = New DataTable()
        Using conn As SqlConnection = New SqlConnection("Server=DESKTOP-KP13QA8\SQLEXPRESS01;Database=HomeBanking;Trusted_Connection=True;")
            Using da As SqlDataAdapter = New SqlDataAdapter(query, conn)
                conn.Open()
                da.Fill(dt)
                conn.Close()
            End Using
            Return dt
        End Using
    End Function

    Sub DoCMD(cmdText As String)
        Using conn As SqlConnection = New SqlConnection("Server=DESKTOP-KP13QA8\SQLEXPRESS01;Database=HomeBanking;Trusted_Connection=True;")
            conn.Open()
            Using cmd As SqlCommand = New SqlCommand(cmdText, conn)
                cmd.ExecuteNonQuery()
                conn.Close()
            End Using
        End Using
    End Sub

    Function GetNextId(tabla As String) As Integer
        Dim dt As DataTable = DoQuery("SELECT MAX(id) FROM " + tabla)

        Return Convert.ToInt32(dt.Rows.Item(0).Item(0)) + 1
    End Function
End Module
