﻿Public Class frmInversiones
    Dim user As user
    Dim plazo As Integer
    Dim interes As Double
    Dim capital As Double
    Dim uva As Boolean
    Dim uvaHoy As Double
    Public Sub Init(us As Object)
        user = CType(us, user)
    End Sub

    Private Sub frmInversiones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        OcultarDetalles()
        crearPlazos(False)
        uvaHoy = getUVA()
        txtUVA.Text = "$ " & uvaHoy
    End Sub

    Sub crearPlazos(uva As Boolean)
        Dim plazos As List(Of String) = New List(Of String)
        If Not uva Then
            plazos.Add("30 días")
            plazos.Add("45 días")
            plazos.Add("60 días")
        End If
        plazos.Add("90 días")
        plazos.Add("120 días")
        plazos.Add("180 días")
        plazos.Add("365 días")
        cbPlazo.DataSource = plazos
    End Sub

    Private Sub cmdDetalles_Click(sender As Object, e As EventArgs) Handles cmdDetalles.Click
        'Fijarme si esta bien
        uva = optUVA.Checked
        If CDbl(txtCapital.Text) < 10000 And Not uva Or CDbl(txtCapital.Text) < 1000 And uva Then
            If uva Then
                MessageBox.Show("Error, el minimo monto es 1000")
            Else
                MessageBox.Show("Error, el minimo monto es 10000")
            End If
            Exit Sub
        End If
        If CDbl(txtCapital.Text) > user.saldoPesos Then
            MessageBox.Show("Error, no posee ese dinero.")
            Exit Sub
        End If
        'todo ok
        capital = CDbl(Val(txtCapital.Text))
        lblMonto.Text = "$" & txtCapital.Text
        lblDias.Text = cbPlazo.SelectedValue
        Dim hoy = Today
        Dim diasDelPlazo As Integer = CInt(cbPlazo.SelectedValue.ToString().Substring(0, 3))
        plazo = diasDelPlazo
        Dim tna As Double = 0
        If diasDelPlazo = 30 Then
            tna = 18.0
        ElseIf diasDelPlazo = 45 Then
            tna = 18.0
        ElseIf diasDelPlazo = 60 Then
            tna = 19.0
        ElseIf diasDelPlazo = 90 Then
            tna = 19.0
        ElseIf diasDelPlazo = 120 Then
            tna = 21.5
        ElseIf diasDelPlazo = 180 Then
            tna = 23.5
        ElseIf diasDelPlazo = 365 Then
            tna = 26.5
        End If
        Dim dia_fin = hoy.AddDays(diasDelPlazo)
        lblFecha.Text = dia_fin.ToString("dd/MM/yyyy")
        If Not uva Then
            lblMontoF.Visible = True
            Label9.Visible = True
            interes = capital * (tna / 100) * (diasDelPlazo / 365)
            Dim montof As Double = capital + interes
            lblMontoF.Text = "$" & montof.ToString("F")
        Else
            lblMontoF.Visible = False
            Label9.Visible = False
        End If
        mostrarDetalles()

    End Sub

    Private Sub cmdCancelar_Click(sender As Object, e As EventArgs) Handles cmdCancelar.Click
        OcultarDetalles()
    End Sub

    Private Sub cmdConfirmar_Click(sender As Object, e As EventArgs) Handles cmdConfirmar.Click
        Dim i As Inversion = New Inversion()
        i.idusuario = user.id
        If uva Then
            i.monto = capital / uvaHoy
        Else
            i.monto = capital
        End If
        i.Plazo = plazo
        i.total = capital + interes
        i.uva = uva
        HandleInversion(i)
        MessageBox.Show("El plazo fijo ha sido confirmado. (Lo puede observar en el modulo 'mis inversiones')")
        OcultarDetalles()
    End Sub


    Sub mostrarDetalles()
        gbDetalles.Visible = True
        Height = 380
    End Sub

    Sub OcultarDetalles()
        gbDetalles.Visible = False
        Height = 210
    End Sub

    Private Sub optNormal_CheckedChanged(sender As Object, e As EventArgs) Handles optNormal.CheckedChanged
        lblTEU.Visible = False
        txtTotalUVA.Visible = False
        lblUva.Visible = False
        txtUVA.Visible = False
        crearPlazos(False)
    End Sub

    Private Sub optUVA_CheckedChanged(sender As Object, e As EventArgs) Handles optUVA.CheckedChanged
        lblTEU.Visible = True
        txtTotalUVA.Visible = True
        lblUva.Visible = True
        txtUVA.Visible = True
        crearPlazos(True)
    End Sub

    Private Sub txtCapital_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCapital.KeyPress
        If Not ((Asc(e.KeyChar)) = 8 Or Asc(e.KeyChar) = 46 Or (Asc(e.KeyChar) >= 48 And Asc(e.KeyChar) <= 57)) Then
            e.KeyChar = Nothing
        End If
    End Sub

    Private Sub txtCapital_TextChanged(sender As Object, e As EventArgs) Handles txtCapital.TextChanged
        txtTotalUVA.Text = "$ " & (CDbl(Val(txtCapital.Text)) / uvaHoy).ToString("F")
    End Sub

    Private Sub txtTotalUVA_TextChanged(sender As Object, e As EventArgs) Handles txtTotalUVA.TextChanged

    End Sub
End Class