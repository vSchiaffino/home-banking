﻿Public Class frmPrincipal
    Dim user As user = New user()
    Dim frmActivo As Form

    Public Sub Init(us As Object)
        user = CType(us, user)
    End Sub

    Private Sub frmPrincipal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Text = "Bienvenido a su homebanking, cuenta: " & user.cuenta
    End Sub

    Private Sub CompraVentaDeDolaresToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CompraVentaDeDolaresToolStripMenuItem.Click
        If Not frmActivo Is Nothing Then frmActivo.Close()
        Dim frm = New frmCompraVenta()
        frmActivo = frm
        frm.Init(user)
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub InversionesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InversionesToolStripMenuItem.Click
        If Not frmActivo Is Nothing Then frmActivo.Close()
        Dim frm = New frmInversiones()
        frmActivo = frm
        frm.Init(user)
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub MisInversionesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MisInversionesToolStripMenuItem.Click
        If Not frmActivo Is Nothing Then frmActivo.Close()
        Dim frm = New frmMisInversiones()
        frmActivo = frm
        frm.Init(user)
        frm.MdiParent = Me
        frm.Show()
    End Sub
End Class