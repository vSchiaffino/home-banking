﻿Module inversionesDAO
    Function GetInversionesEnCurso(idusuario As Integer) As List(Of Inversion)
        Return LlenarDTO(DoQuery("SELECT * FROM inversiones WHERE estado = 0 and idusuario = " & idusuario))
    End Function

    Function GetInversionesTerminadas(idusuario As Integer) As List(Of Inversion)
        Return LlenarDTO(DoQuery("SELECT * FROM inversiones WHERE estado = 1 and idusuario = " & idusuario))
    End Function

    Sub HandleInversion(i As Inversion)
        Dim id = GetNextId("inversiones")
        i.FechaInicio = Today
        i.FechaFinal = i.FechaInicio.AddDays(i.Plazo)
        If i.uva Then
            i.total = 0
        End If
        Dim cmdtext = "INSERT INTO inversiones(id, idusuario, uva, monto, fechainicio, fechafinal, plazo, total) VALUES (" & id & ", " & i.idusuario & ", " & Convert.ToInt32(i.uva) & ", " & i.monto.ToString().Replace(",", ".") & ", '" & i.FechaInicio.ToString("dd-MM-yyyy") & "', '" & i.FechaFinal.ToString("dd-MM-yyyy") & "', " & i.Plazo & ", " & i.total.ToString().Replace(",", ".") & ")"
        DoCMD(cmdtext)
        ActualizarPesos(i.idusuario, -i.monto)
    End Sub

    Sub TerminarInversion(i As Inversion)
        Dim query = "UPDATE inversiones SET total = " & i.total.ToString().Replace(",", ".") & ", estado = 1 WHERE id = " & i.id
        ActualizarPesos(i.idusuario, i.total)
        DoCMD(query)
    End Sub

    Function GetInversion(id As Integer)
        Return LlenarDTO(DoQuery("SELECT * FROM inversiones WHERE id = " & id)).Item(0)
    End Function

    Function LlenarDTO(dt As DataTable) As List(Of Inversion)
        Dim inversiones As List(Of Inversion) = New List(Of Inversion)
        For Each dr As DataRow In dt.Rows
            Dim i As Inversion = New Inversion
            i.id = Convert.ToInt32(dr.Item("id"))
            i.idusuario = Convert.ToInt32(dr.Item("idusuario"))
            i.uva = Convert.ToBoolean(dr.Item("uva"))
            i.monto = Convert.ToDouble(dr.Item("monto"))
            i.FechaInicio = Convert.ToDateTime(dr.Item("FechaInicio"))
            i.FechaFinal = Convert.ToDateTime(dr.Item("FechaFinal"))
            i.Plazo = Convert.ToInt32(dr.Item("Plazo"))
            i.estado = Convert.ToBoolean(dr.Item("estado"))
            i.total = Convert.ToDouble(dr.Item("total"))
            inversiones.Add(i)
        Next
        Return inversiones
    End Function
End Module
