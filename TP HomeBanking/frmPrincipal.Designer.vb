﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrincipal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.HomeBankingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CompraVentaDeDolaresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InversionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MisInversionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HomeBankingToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(992, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'HomeBankingToolStripMenuItem
        '
        Me.HomeBankingToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CompraVentaDeDolaresToolStripMenuItem, Me.InversionesToolStripMenuItem, Me.MisInversionesToolStripMenuItem})
        Me.HomeBankingToolStripMenuItem.Name = "HomeBankingToolStripMenuItem"
        Me.HomeBankingToolStripMenuItem.Size = New System.Drawing.Size(98, 20)
        Me.HomeBankingToolStripMenuItem.Text = "Home banking"
        '
        'CompraVentaDeDolaresToolStripMenuItem
        '
        Me.CompraVentaDeDolaresToolStripMenuItem.Name = "CompraVentaDeDolaresToolStripMenuItem"
        Me.CompraVentaDeDolaresToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.CompraVentaDeDolaresToolStripMenuItem.Text = "Compra venta de dolares"
        '
        'InversionesToolStripMenuItem
        '
        Me.InversionesToolStripMenuItem.Name = "InversionesToolStripMenuItem"
        Me.InversionesToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.InversionesToolStripMenuItem.Text = "Invertir"
        '
        'MisInversionesToolStripMenuItem
        '
        Me.MisInversionesToolStripMenuItem.Name = "MisInversionesToolStripMenuItem"
        Me.MisInversionesToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.MisInversionesToolStripMenuItem.Text = "Mis inversiones"
        '
        'frmPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(992, 631)
        Me.Controls.Add(Me.MenuStrip1)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmPrincipal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmPrincipal"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents HomeBankingToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CompraVentaDeDolaresToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents InversionesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MisInversionesToolStripMenuItem As ToolStripMenuItem
End Class
